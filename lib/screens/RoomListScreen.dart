import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import '../info_storage/RoomInfo.dart';
import './MdnsScreen.dart';
import './RoomScreen.dart';

import '../global.dart';

class RoomListScreen extends StatelessWidget {
  const RoomListScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    RoomListInfo info = Global['roomListInfo'];
    String title = 'Rooms';
    TextEditingController _roomName;
    TextEditingController _roomMemo;
    return ScopedModel<RoomListInfo>(
        model: info,
        child: Scaffold(
          appBar: AppBar(
            title: Text(title),
            actions: <Widget>[
              IconButton(
                tooltip: 'Rooms',
                icon: Icon(Icons.home),
                onPressed: () {
                  print('rooms');
                },
              ),
              // action button
              IconButton(
                tooltip: 'favourites',
                icon: Icon(Icons.star_border),
                onPressed: () {
                  print('favorites');
                },
              ),
              PopupMenuButton(
                onSelected: (String value) {
                  print('selected $value');
                  if (value == 'mdns') {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MdnsScreen()),
                    );
                  }
                },
                itemBuilder: (BuildContext context) => <PopupMenuItem<String>>[
                      const PopupMenuItem<String>(
                        value: 'mdns',
                        child: Text('Find devices'),
                      ),
                    ],
              )
            ],
          ),
          body: ScopedModelDescendant<RoomListInfo>(builder: (context, child, model) {
            List<RoomInfo> rooms = model.rooms;
            ListView roomsListView = ListView.builder(
                itemCount: rooms.length,
                itemBuilder: (BuildContext ctx, int index) {
                  List<Widget> actions = new List<Widget>();
                  List<Widget> secondaryActions = new List<Widget>();

                  // do not show button on default
                  if (index > 0) {
                    Widget editButton = new IconSlideAction(
                        caption: 'Edit',
                        color: Colors.blueGrey,
                        icon: Icons.edit,
                        onTap: () {
                          return showDialog(
                            context: context,
                            builder: (context) {
                              _roomName = new TextEditingController(text: rooms[index].name);
                              _roomMemo = new TextEditingController(text: rooms[index].memo);
                              return new Dialog(
                                child: new Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Text("Room name: "),
                                    new TextField(
                                      autofocus: true,
                                      decoration: new InputDecoration(hintText: 'Default room'),
                                      controller: _roomName,
                                    ),
                                    new Text("Room memo: "),
                                    new TextField(
                                      decoration: new InputDecoration(hintText: 'Default memo. Try #hello'),
                                      controller: _roomMemo,
                                    ),
                                    new FlatButton(
                                      child: new Text("Save"),
                                      onPressed: () async {
                                        String roomName = _roomName.text;
                                        String roomMemo = _roomMemo.text;
                                        if (roomName.length > 0) {
                                          RoomInfo room = new RoomInfo(roomName, 'red', roomMemo);
                                          await model.replaceRoom(index, room);
                                          Navigator.pop(context);
                                        } else {
                                          print("name len is 0");
                                        }
                                      },
                                    )
                                  ],
                                ),
                              );
                            },
                          );
                        });
                    Widget deleteButton = new IconSlideAction(
                        caption: 'Delete',
                        color: Colors.red,
                        icon: Icons.delete,
                        onTap: () {
                          return showDialog(
                            context: context,
                            builder: (context) {
                              return new AlertDialog(
                                  title: new Text('Remove room?'),
                                  content: new Text('You are going to delete room: ${rooms[index].name}. Sure?'),
                                  actions: <Widget>[
                                    new FlatButton(
                                      child: new Text("Yep"),
                                      onPressed: () async {
                                        await model.removeRoom(index);
                                        // TODO: move accessories from this room to default
                                        Navigator.pop(context);
                                      },
                                    ),
                                    new FlatButton(
                                      child: new Text("No"),
                                      onPressed: () async {
                                        Navigator.pop(context);
                                      },
                                    )
                                  ]);
                            },
                          );
                        });
                    actions.add(editButton);
                    secondaryActions.add(deleteButton);
                  }

                  return new Slidable(
                      delegate: new SlidableDrawerDelegate(),
                      actionExtentRatio: 0.25,
                      child: new Container(
                        child: new ListTile(
                          leading: new CircleAvatar(
                            backgroundColor: Colors.indigoAccent,
                            child: new Text('${rooms[index].name[0]}'),
                            foregroundColor: Colors.white,
                          ),
                          title: new Text('${rooms[index].name}'),
                          subtitle: new Text(rooms[index].memo),
                          onTap: () {
                            // TODO: open room screen
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => RoomScreenWidget(room: rooms[index])),
                            );
                          },
                        ),
                      ),
                      actions: actions,
                      secondaryActions: secondaryActions);
                });
            return roomsListView;
          }),
          floatingActionButton: new FloatingActionButton(
            onPressed: () {
              return showDialog(
                context: context,
                builder: (context) {
                  _roomName = new TextEditingController();
                  _roomMemo = new TextEditingController();
                  return new Dialog(
                    child: new Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Text("Room name: "),
                        new TextField(
                          autofocus: true,
                          decoration: new InputDecoration(hintText: 'Default room'),
                          controller: _roomName,
                        ),
                        new Text("Room memo: "),
                        new TextField(
                          decoration: new InputDecoration(hintText: 'Default memo. Try #hello'),
                          controller: _roomMemo,
                        ),
                        new FlatButton(
                          child: new Text("Add"),
                          onPressed: () async {
                            String roomName = _roomName.text;
                            String roomMemo = _roomMemo.text;
                            if (roomName.length > 0) {
                              // TODO: color
                              RoomInfo newRoom = new RoomInfo(roomName, 'red', roomMemo);
                              await info.addRoom(newRoom);
                              Navigator.pop(context);
                            } else {
                              print('invalid room name len');
                            }
                          },
                        )
                      ],
                    ),
                  );
                },
              );
            },
            child: new Icon(Icons.plus_one),
            tooltip: 'Add new room',
          ),
        ));
  }
}
