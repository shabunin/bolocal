// this file contains code for mdns screen

// TODO: list of discovered devices.
// TODO: also think about already paired devices. where is info???
// TODO: if device is paired then show checkbox list.
// TODO: on checkbox tap:
// TODO:   if not paired yet,
// TODO:     pair, add devices to Default room.
// TODO:   else, if paired, then
// TODO:     show dialog asking are u sure to unpair?

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../classes/AccessoryInfo.dart';
import '../info_storage/RoomInfo.dart';
import '../classes/MdnsExplorer.dart';
import '../global.dart';

class MdnsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MdnsExplorer explorer = Global['mdnsExplorer'];
    // TODO: implement build
    return new ScopedModel<MdnsExplorer>(
        model: explorer,
        child: new Scaffold(
          appBar: new AppBar(title: new Text('mdns')),
          body: new ScopedModelDescendant<MdnsExplorer>(builder: (context, child, model) {
            ListView mdnsListView = ListView.builder(
                itemCount: model.discovered.length,
                itemBuilder: (BuildContext ctx, int index) {
                  //                  List<Widget> actions = new List<Widget>();
                  //                  List<Widget> secondaryActions = new List<Widget>();

                  // do not show button on default
                  // TODO: info: paired or not
                  bool devicePaired = model.discovered[index].paired;
                  return ListTile(
                    leading: new Icon(Icons.computer),
                    title: new Text(model.discovered[index].name),
                    subtitle: new Text('paired: $devicePaired'),
                    onTap: () {
                      if (devicePaired) {
                        // TODO: show dialog askin to really unpair??
                        Global['events'].emit('unpair', [model.discovered[index]]);
                      } else {
                        // TODO: pair device
                        Global['events'].emit('pair', [model.discovered[index]]);
                      }
                    },
                  );
                });
            return mdnsListView;
          }),
        ));
  }
}
