// show room accessories
// if room is default, then also snow sort by rooms in popup

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../classes/AccessoryInfo.dart';
import '../classes/BobaosKit.dart';
import '../info_storage/RoomInfo.dart';

import '../global.dart';

import './widgets/switch.dart';
import './widgets/radioPlayer.dart';
import './widgets/temperatureSensor.dart';
import './widgets/thermostat.dart';

class RoomScreenWidget extends StatelessWidget {
  const RoomScreenWidget({Key key, this.room}) : super(key: key);
  final RoomInfo room;

  @override
  Widget build(BuildContext context) {
    RoomListInfo roomListInfo = Global['roomListInfo'];
    int defaultRoomIndex = roomListInfo.rooms.indexWhere((f) => f.name == 'Default');
    List<String> defaultRoomAccessories = new List<String>();
    if (defaultRoomIndex > -1) {
      defaultRoomAccessories = roomListInfo.rooms[defaultRoomIndex].accessories;
    }
    List<AccessoryInfo> accessories = Global['accessories'];
    List<BobaosKitWs> kits = Global['kits'];

    bool isDefaultRoom = room.name == 'Default';

    int currentRoomIndex = roomListInfo.rooms.indexWhere((f) => f.name == room.name);
    List<String> currentRoomAccessories = new List<String>();
    if (currentRoomIndex > -1) {
      currentRoomAccessories = roomListInfo.rooms[currentRoomIndex].accessories;
    }

    // TODO: implement build
    return new Scaffold(
        appBar: new AppBar(
            title: new Text(room.name),
            leading: PopupMenuButton(
              onSelected: (String value) {
                print('selected $value');
                if (value == 'add_accessories') {
                  // TODO: show dialog with checkbox list
                  // TODO: move accessories from default room on 'Yep'
                  showDialog(
                      context: context,
                      builder: (context) {
                        return new Dialog(
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Expanded(
                                  child: new ScopedModel<RoomListInfo>(
                                      model: roomListInfo,
                                      child: new ScopedModelDescendant<RoomListInfo>(builder: (context, child, model) {
                                        return ListView.builder(
                                          itemCount: defaultRoomAccessories.length + currentRoomAccessories.length,
                                          itemBuilder: (BuildContext context, int i) {
                                            String acc;
                                            if (i < defaultRoomAccessories.length) {
                                              acc = defaultRoomAccessories[i];
                                            } else {
                                              acc = currentRoomAccessories[i - defaultRoomAccessories.length];
                                            }
                                            bool v = (!defaultRoomAccessories.contains(acc) &&
                                                currentRoomAccessories.contains(acc));
                                            return CheckboxListTile(
                                              // TODO: state?
                                              value: v,
                                              title: new Text(acc),
                                              onChanged: (bool value) {
                                                if (value) {
                                                  roomListInfo.rooms[defaultRoomIndex].removeAccessory(acc);
                                                  roomListInfo.rooms[currentRoomIndex].addAccessory(acc);
                                                  roomListInfo.notifyListeners();
                                                  roomListInfo.writeFile();
                                                } else {
                                                  roomListInfo.rooms[defaultRoomIndex].addAccessory(acc);
                                                  roomListInfo.rooms[currentRoomIndex].removeAccessory(acc);
                                                  roomListInfo.notifyListeners();
                                                  roomListInfo.writeFile();
                                                }
                                              },
                                            );
                                          },
                                        );
                                      }))),
//                              new FlatButton(
//                                  onPressed: () {
//                                    print('add');
//                                    if (currentRoomIndex > -1) {
//                                      Navigator.pop(context);
//                                    }
//                                  },
//                                  child: new Text('Add'))
                            ],
                          ),
                        );
                      });
                }
              },
              itemBuilder: (BuildContext context) {
                List<PopupMenuItem<String>> result = new List<PopupMenuItem<String>>();
                if (!isDefaultRoom) {
                  result.add(const PopupMenuItem<String>(
                    value: 'add_accessories',
                    child: Text('Add accessories'),
                  ));
                }
                return result;
              },
            )),
        body: new ScopedModel<RoomInfo>(
            model: room,
            child: new ScopedModelDescendant<RoomInfo>(builder: (context, child, model) {
              return new ListView.builder(
                  itemCount: model.accessories.length,
                  itemBuilder: (BuildContext context, int index) {
                    // TODO: find each accessory in global array
                    // split accessory, written in following format: "bobaoskit://acc_id"
                    String accessoryUri = model.accessories[index];
                    print('room ${model.name} acc: $accessoryUri');
                    List<String> parsed = accessoryUri.split('://');
                    print(parsed);

                    String bobaosId = parsed[0];
                    int globalKitIndex = kits.indexWhere((BobaosKitWs bk) {
                      return bk.name == bobaosId;
                    });

                    String accessoryId = parsed[1];
                    int globalAccIndex = accessories.indexWhere((AccessoryInfo f) {
                      return f.id == accessoryId && f.bobaos_id == bobaosId;
                    });
                    print('indexes: $globalAccIndex, $globalKitIndex');
                    if (globalAccIndex > -1 && globalKitIndex > -1) {
                      AccessoryInfo info = accessories[globalAccIndex];
                      BobaosKitWs bobaoskit = kits[globalKitIndex];
                      if (info.type == "switch") {
                        return AccSwitch(
                          info: info,
                          bobaos: bobaoskit,
                        );
                      }
                      if (info.type == "temperature sensor") {
                        return AccTemperatureSensor(
                          info: info,
                          bobaos: bobaoskit,
                        );
                      }
                      if (info.type == "radio player") {
                        return AccRadioPlayer(info: info, bobaos: bobaoskit);
                      }
                      if (info.type == "thermostat") {
                        return AccThermostat(
                          info: info,
                          bobaos: bobaoskit,
                        );
                      }
                    }
                    // TODO: build its model
                    // TODO: find bobaoskit in global array
                  });
            })));
  }
}
