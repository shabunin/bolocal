// Class for accessory model

import 'package:scoped_model/scoped_model.dart';

// AccessoryInfo extends Model
// so, when accessory value is updated it descends down to
// all widgets inside ScopedModelDescendant
class AccessoryInfo extends Model {
  // from bobaoskit
  dynamic id;
  dynamic type;
  String name;
  String job_channel;
  List control;
  List status;

  // current state
  Map<dynamic, dynamic> currentState;

  // additional params
  dynamic bobaos_id;

  AccessoryInfo(Map<dynamic, dynamic> obj) {
    this.bobaos_id = obj['bobaos_id'];
    this.id = obj['id'];
    this.type = obj['type'];
    this.name = obj['name'];
    this.job_channel = obj['job_channel'];
    this.control = obj['control'];
    this.status = obj['status'];
    this.currentState = {};
  }

  void updateCurrentState(key, value) {
    currentState[key] = value;
    notifyListeners();
  }

  void notify() {
    notifyListeners();
  }

  Map toJson() {
    Map map = new Map();
    map['bobaos_id'] = this.bobaos_id;
    map['id'] = this.id;
    map['type'] = this.type;
    map['name'] = this.name;
    map['control'] = this.control;
    map['status'] = this.status;
//    map['current_state'] = this.currentState;

    return map;
  }
}
