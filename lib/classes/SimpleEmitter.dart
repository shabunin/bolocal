// simple implementation of EventEmitter

class SimpleEmitter {
  // event name, list of callbacks
  Map<String, List<Function>> _listeners = {};
  //  Map<dynamic, List> _oneTimeListeners = {};
  SimpleEmitter() {
    _listeners = {};
  }

  void addListener(String event, Function cb) {
    if (!_listeners.containsKey(event)) {
      _listeners[event] = new List<Function>();
    }
    _listeners[event].add(cb);
  }

  void emit(String event, [List params = const []]) {
    if (_listeners.containsKey(event)) {
      _listeners[event].forEach((f) {
        if (f is Function) {
          Function.apply(f, params);
        }
      });
    }
  }

  int removeListener(String event, Function cb) {
    if (_listeners.containsKey(event)) {
      int i = _listeners[event].indexWhere((f) {
        return f == cb;
      });
      if (i > -1) {
        _listeners[event].removeAt(i);
      }
    }
  }

  void removeAllListeners(String event) {
    if (_listeners.containsKey(event)) {
      _listeners.remove(event);
    }
  }

  void clearListeners() {
    _listeners = {};
  }
}
