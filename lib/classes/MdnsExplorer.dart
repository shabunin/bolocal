// this file is responsible for mdns discovery
// emits events when service is resolved/lost
import 'package:mdns/mdns.dart';
import 'package:scoped_model/scoped_model.dart';
import './SimpleEmitter.dart';

class ServicePairedInfo extends ServiceInfo {
  bool paired;

  ServicePairedInfo(String name, String type, String host, int port, Map txtRecords)
      : super(name, type, host, port, txtRecords);
}

class MdnsExplorer extends Model {
  String service;
  Mdns mdns;
  SimpleEmitter emitter;
  DiscoveryCallbacks discoveryCallbacks;

  // store discovered services
  List<ServicePairedInfo> discovered;

  start() {
    mdns.startDiscovery(service);
  }

  stop() {
    mdns.stopDiscovery();
  }

  MdnsExplorer([String service = '_bobaoskit._tcp']) {
    this.emitter = new SimpleEmitter();
    this.service = service;
    discovered = new List<ServicePairedInfo>();

    discoveryCallbacks = new DiscoveryCallbacks(onDiscoveryStarted: () {
      // do noting
    }, onDiscoveryStopped: () {
      // do noting
    }, onDiscovered: (ServiceInfo info) {
      // do noting
    }, onResolved: (ServiceInfo info) {
      // insert in list if doesn't exist
      int index = discovered.indexWhere((ServicePairedInfo f) {
        return (f.name == info.name && f.host == info.host && f.port == info.port);
      });
      if (index < 0) {
        ServicePairedInfo _info = new ServicePairedInfo(info.name, info.type, info.host, info.port, info.txtRecords);
        _info.paired = false;
        discovered.add(_info);
        this.emitter.emit('resolved', [_info]);
        notifyListeners();
      }
    }, onLost: (ServiceInfo info) {
      print('service lost: ' + info.toString());
      // remove from list then
      int index = discovered.indexWhere((ServicePairedInfo f) {
        return f.name == info.name;
      });
      if (index > -1) {
        discovered.removeAt(index);
        this.emitter.emit('lost', [info]);
        notifyListeners();
      }
    });
    mdns = new Mdns(discoveryCallbacks: discoveryCallbacks);
//    mdns.startDiscovery(service);
  }
}
