import 'dart:async';
import 'dart:convert';
import 'dart:io';

import './SimpleEmitter.dart';

class BobaosKitWs extends SimpleEmitter {
  String name;
  String url;
  WebSocket ws;

  int _req_count;
  Map _reqs;

  // callbacks for bcasted events

  void initWs() async {
    try {
      this.ws = await WebSocket.connect(this.url);
      // on incoming data
      void onData(text) {
        var json = jsonDecode(text);
        if (json.containsKey('response_id')) {
          var response_id = json['response_id'];
          if (this._reqs.containsKey(response_id)) {
            var cb = this._reqs[response_id];
            if (json['method'] == 'success') {
              cb(false, json['payload']);
            } else {
              cb(true, json['payload']);
            }
            this._reqs.remove(response_id);
          }
        } else {
          // broadcasted event
          this.emit(json['method'], [json['payload']]);
        }
      }

      void onError(error) {
        print('error with  websocket: ' + error.toString());
        this.emit('error', [error]);
      }

      void onDone() {
        print('websocket done');
        this.emit('done');
      }

      this.ws.listen(onData, onDone: onDone, onError: onError);
      this.emit('ready');
    } catch (e) {
      this.emit('error', [e]);
      throw e;
    }
  }

  void closeWs() async {
    // remove all listeners from all events
    this.clearListeners();
    await this.ws.close();
  }

  void listen() async {}

  Future<dynamic> getGeneralInfo() async {
    var completer = new Completer();

    this._req_count += 1;
    int request_id = this._req_count;
    var obj2send = {};
    obj2send["request_id"] = request_id;
    obj2send["method"] = "get general info";
    obj2send["payload"] = null;
    this.ws.add(jsonEncode(obj2send));

    // callback to store
    void cb(bool err, dynamic payload) {
      if (err) {
        completer.completeError(payload.toString());
        return;
      }

      completer.complete(payload);
    }

    this._reqs[request_id] = cb;

    return completer.future;
  }

  Future<dynamic> getAccessoryInfo(dynamic id) async {
    var completer = new Completer();

    this._req_count += 1;
    int request_id = this._req_count;
    var obj2send = {};
    obj2send["request_id"] = request_id;
    obj2send["method"] = "get accessory info";
    obj2send["payload"] = id;
    this.ws.add(jsonEncode(obj2send));

    void cb(bool err, dynamic payload) {
      if (err) {
        completer.completeError(payload.toString());
        return;
      }

      completer.complete(payload);
    }

    this._reqs[request_id] = cb;

    return completer.future;
  }

  Future<dynamic> getStatusValue(dynamic id, dynamic status) async {
    var completer = new Completer();

    this._req_count += 1;
    int request_id = this._req_count;
    var obj2send = {};
    obj2send["request_id"] = request_id;
    obj2send["method"] = 'get status value';
    var payload = {};
    payload["id"] = id;
    List<Object> statusList = [];
    if (status is List) {
      status.forEach((e) {
        statusList.add(e);
      });
      payload['status'] = statusList;
    } else {
      payload['status'] = status;
    }
    obj2send["payload"] = payload;
    this.ws.add(jsonEncode(obj2send));

    void cb(bool err, dynamic payload) {
      if (err) {
        completer.completeError(payload.toString());
        return;
      }

      completer.complete(payload);
    }

    this._reqs[request_id] = cb;

    return completer.future;
  }

  Future<dynamic> controlAccessoryValue(dynamic id, Map<dynamic, dynamic> value) async {
    var completer = new Completer();

    this._req_count += 1;
    int request_id = this._req_count;
    var obj2send = {};
    obj2send["request_id"] = request_id;
    obj2send["method"] = "control accessory value";
    List<Object> valueList = [];
    value.forEach((k, v) {
      valueList.add({'field': k, 'value': v});
    });
    var payload = {};
    payload["id"] = id;
    if (valueList.length == 1) {
      payload["control"] = valueList[0];
    } else {
      payload["control"] = valueList;
    }
    obj2send["payload"] = payload;
    this.ws.add(jsonEncode(obj2send));

    void cb(bool err, dynamic payload) {
      if (err) {
        completer.completeError(payload.toString());
        return;
      }

      completer.complete(payload);
    }

    this._reqs[request_id] = cb;

    return completer.future;
  }

  BobaosKitWs(String name, String url) {
    this.name = name;
    this.url = url;
    this._reqs = new Map<int, Function>();
    this._req_count = 0;
  }
}
