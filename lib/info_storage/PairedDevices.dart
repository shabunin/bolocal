// this source serves to read/write from file info about paired devices

import 'dart:io';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import 'package:scoped_model/scoped_model.dart';

class PairedDeviceesInfo extends Model {
  String filename;
  File file;
  List<String> paired;

  PairedDeviceesInfo(String filename) {
    this.filename = filename;
    paired = new List<String>();
  }

  Future<File> getFile() async {
    try {
      // get the path to the document directory.
      String dir = (await getApplicationDocumentsDirectory()).path;
      String path = '$dir/$filename';

      file = new File(path);

      if (await FileSystemEntity.type(path) == FileSystemEntityType.notFound) {
        print('file not found, creating new');
        return await file.writeAsString(jsonEncode(paired));
      }
      if (await FileSystemEntity.type(path) == FileSystemEntityType.file) {
        print('file exists, great');

        return file;
      }
      if (await FileSystemEntity.type(path) == FileSystemEntityType.directory) {
        print('file exists, but it is directory, not great');
        await file.delete(recursive: true);

        return await getFile();
      }
    } catch (e) {
      print('error while getting devices info file: ' + e.toString());
    }
  }

  Future<List<String>> readFile() async {
    try {
      print(await file.readAsString());
      // loaded dynamic list of devices
      List<dynamic> _content = jsonDecode(await file.readAsString());

      // clear current list
      paired.clear();
      // process dynamic list
      _content.forEach((t) {
        paired.add(t);
      });

      notifyListeners();
    } catch (e) {
      print('error while reading devices info file: ' + e.toString());
    }
  }

  Future<File> writeFile() async {
    try {
      String _json = jsonEncode(paired);
      print('writing $_json');

      return file.writeAsString(_json);
    } catch (e) {
      print('error while writing devices info file: ' + e.toString());
    }
  }

  Future<List<String>> add(String name) async {
    int index = paired.indexWhere((f) {
      return f == name;
    });
    if (index == -1) {
      paired.add(name);
      notifyListeners();
      await writeFile();
    } else {
      print('id exists in array, do not add');
    }
    return paired;
  }

  Future<List<String>> remove(String name) async {
    int index = paired.indexWhere((f) {
      return f == name;
    });
    if (index > -1) {
      paired.removeAt(index);
      notifyListeners();
      await writeFile();
    } else {
      print('cant find device in paired list');
    }
    return paired;
  }
}
