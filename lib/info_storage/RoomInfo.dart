// this source serves to read/write from file room info and store in memory
// room list is a scoped model, so at any change all descendants are notified

import 'dart:io';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import 'package:scoped_model/scoped_model.dart';

class RoomInfo extends Model {
  String name;
  String color;
  String memo;

  // accessory list
  List<String> accessories;

  RoomInfo(String name, String color, String memo) {
    this.name = name;
    this.color = color;
    this.memo = memo;

    // create empty accessory list
    this.accessories = new List<String>();
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is RoomInfo && name == other.name;

  addAccessory(String accName) {
    int i = accessories.indexWhere((a) => a == accName);
    if (i == -1) {
      accessories.add(accName);
      notifyListeners();
    } else {
      //  already added
    }
  }

  removeAccessory(String accName) {
    accessories.removeWhere((a) => a == accName);
    notifyListeners();
  }

  Map toJson() {
    Map map = new Map();
    map['name'] = this.name;
    map['color'] = this.color;
    map['memo'] = this.memo;
    map['accessories'] = this.accessories;

    return map;
  }
}

class RoomListInfo extends Model {
  String filename;
  File file;
  List<RoomInfo> rooms;

  RoomListInfo(String filename) {
    this.filename = filename;
    rooms = new List<RoomInfo>();

    RoomInfo defaultRoom = new RoomInfo('Default', 'red', 'Place for unsorted accsessories.');
    defaultRoom.memo = "Place for unsorted accessories";
    defaultRoom.accessories = new List<String>();
    rooms.add(defaultRoom);
  }

  Future<File> getFile() async {
    try {
      // get the path to the document directory.
      String dir = (await getApplicationDocumentsDirectory()).path;
      String path = '$dir/$filename';

      file = new File(path);

      if (await FileSystemEntity.type(path) == FileSystemEntityType.notFound) {
        print('file not found, creating new');
        return await file.writeAsString(jsonEncode(rooms));
      }
      if (await FileSystemEntity.type(path) == FileSystemEntityType.file) {
        print('file exists, great');

        return file;
      }
      if (await FileSystemEntity.type(path) == FileSystemEntityType.directory) {
        print('file exists, but it is directory, not great');
        await file.delete(recursive: true);

        return await getFile();
      }
    } catch (e) {
      print('error while getting room info file: ' + e.toString());
    }
  }

  Future<List<RoomInfo>> readFile() async {
    try {
      print(await file.readAsString());
      // loaded dynamic list of rooms
      List<dynamic> _roomsDynamic = jsonDecode(await file.readAsString());

      // clear current list
      rooms.clear();
      // process dynamic list
      _roomsDynamic.forEach((t) {
        RoomInfo _r = new RoomInfo(t['name'], t['color'], t['memo']);
        print(t);
        if (t['accessories'] is List) {
          t['accessories'].forEach((a) {
            _r.accessories.add(a.toString());
          });
        }
        rooms.add(_r);
      });

      notifyListeners();
      return rooms;
    } catch (e) {
      print('error while reading room info file: ' + e.toString());
    }
  }

  Future<File> writeFile() async {
    try {
      String _json = jsonEncode(rooms);
      print('writing $_json');

      return file.writeAsString(_json);
    } catch (e) {
      print('error while writing room info file: ' + e.toString());
    }
  }

  Future<List<RoomInfo>> addRoom(RoomInfo newRoom) async {
    int roomIndex = rooms.indexWhere((f) {
      return f.name == newRoom.name;
    });
    if (roomIndex == -1) {
      rooms.add(newRoom);
      notifyListeners();
      await writeFile();
    } else {
      print('id exists in array, do not add');
    }
    return rooms;
  }

  Future<List<RoomInfo>> removeRoom(int index) async {
    // do not permit to delete default room
    if (index > 0 && index < rooms.length) {
      // move accs to default room
      if (rooms[index].accessories.length > 0) {
        int defaultRoomIndex = rooms.indexWhere((r) => r.name == 'Default');
        if (defaultRoomIndex > -1) {
          rooms[index].accessories.forEach((f) {
            print('removing $f');
            rooms[defaultRoomIndex].addAccessory(f);
          });
        }
      }
      rooms.removeAt(index);
      notifyListeners();
      await writeFile();
      return rooms;
    } else {
      print('index out of range');
    }
  }

  Future<List<RoomInfo>> moveRoom(int fromPosition, int toPosition) async {
    RoomInfo room = rooms[fromPosition];
    rooms.insert(toPosition, room);
    rooms.removeAt(fromPosition);
    notifyListeners();
    await writeFile();
  }

  Future<List<RoomInfo>> replaceRoom(int at, RoomInfo room) async {
    int roomIndex = rooms.indexWhere((f) {
      return f.name == room.name;
    });

    if (roomIndex == -1 || roomIndex == at) {
      rooms.removeAt(at);
      rooms.insert(at, room);
      notifyListeners();
      await writeFile();
    }
  }
}
