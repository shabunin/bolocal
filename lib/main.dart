import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:connectivity/connectivity.dart';

import './info_storage/RoomInfo.dart';
import './info_storage/PairedDevices.dart';
import './classes/BobaosKit.dart';
import './classes/SimpleEmitter.dart';

// TODO: refactor everywhere as here:
import 'package:bolocal/screens/RoomListScreen.dart';
import 'package:bolocal/screens/MdnsScreen.dart';
import 'package:bolocal/classes/MdnsExplorer.dart';
import './screens/RoomScreen.dart';

import './classes/AccessoryInfo.dart';

import './global.dart';

void main() async {
  PairedDeviceesInfo pairedDevicesInfo;
  RoomListInfo roomListInfo;
  MdnsExplorer mdnsExplorer;
  SimpleEmitter events;
  List<BobaosKitWs> kits;
  List<AccessoryInfo> accessories;

  Global = new Map<String, dynamic>();

  events = new SimpleEmitter();
  Global['events'] = events;

  roomListInfo = new RoomListInfo('rooms.json');
  Global['roomListInfo'] = roomListInfo;

  await roomListInfo.getFile();
  await roomListInfo.readFile();
  print('got list');

  pairedDevicesInfo = new PairedDeviceesInfo('paired.json');
  Global['pairedDevicesInfo'] = pairedDevicesInfo;
  await pairedDevicesInfo.getFile();
  await pairedDevicesInfo.readFile();
  print('got devices');
  print(pairedDevicesInfo.paired);

  accessories = new List<AccessoryInfo>();
  Global['accessories'] = accessories;

  mdnsExplorer = new MdnsExplorer();
  Global['mdnsExplorer'] = mdnsExplorer;

  Connectivity().onConnectivityChanged.listen((ConnectivityResult result) async {
    // Got a new connectivity status!
    if (result == ConnectivityResult.mobile) {
      mdnsExplorer.stop();
    } else if (result == ConnectivityResult.wifi) {
      mdnsExplorer.start();
    }
  });

  kits = new List<BobaosKitWs>();
  Global['kits'] = kits;

  void pair(ServicePairedInfo info) async {
    if (!info.paired) {
      print('connecting to paired device ${info.name}');
      BobaosKitWs my = new BobaosKitWs(info.name, 'ws:/${info.host}:${info.port}');

      Future<List<AccessoryInfo>> loadAccessories() async {
        try {
          print('trying to run loadAccessories()');
          List<dynamic> tmp = await my.getAccessoryInfo(null);
          List<AccessoryInfo> result = new List<AccessoryInfo>();
          tmp.forEach((f) {
            f['bobaos_id'] = info.name;

            // try to find in global list
            int gindex = accessories.indexWhere((a) {
              return (a.id == f['id'] && a.bobaos_id == f['bobaos_id']);
            });
            print('accessory index is $gindex');
            // if not found
            if (gindex < 0) {
              AccessoryInfo item = new AccessoryInfo(f);
              print('adding accessory to global list');
              accessories.add(item);
              result.add(item);
              // TODO: add to default room
              bool inSomeRoom = false;
              String name = '${f['bobaos_id']}://${f['id']}';
              roomListInfo.rooms.forEach((RoomInfo r) {
                print('${r.name}::${r.accessories}');
                if (r.accessories.contains(name)) {
                  print('room ${r.name} contains acc $name');
                  inSomeRoom = true;
                }
              });
              // add to default room if nowhere found
              if (!inSomeRoom) {
                roomListInfo.rooms[0].addAccessory(name);
              } else {
                print('acc found in some room');
              }
            } else {
              print('accessory already in list');
            }
          });
          return result;
        } catch (e) {
          print('error loading accessory list: ${e.toString()}');
        }
      }

      int timeout = 5;
      void connect() async {
        try {
          await my.initWs();
          List<AccessoryInfo> mylist = await loadAccessories();
          print('acc list loaded');
          mylist.forEach((f) async {
            print(f.toJson());
            // get status value at start
            dynamic payload = await my.getStatusValue(f.id, f.status);
            dynamic statusValues = payload['status'];
            if (statusValues is Map) {
              dynamic field = statusValues['field'];
              dynamic value = statusValues['value'];
              f.updateCurrentState(field, value);
            }
            if (statusValues is List) {
              statusValues.forEach((statusValue) {
                if (statusValue is Map) {
                  dynamic field = statusValue['field'];
                  dynamic value = statusValue['value'];
                  f.updateCurrentState(field, value);
                }
              });
            }
          });
          print('default room accs:');
          print(roomListInfo.rooms[0].accessories.toString());
        } catch (e) {
          print('there was error with connection: ' + e.toString());
          print('reconnecting in ${timeout} seconds');
          var future = new Future.delayed(Duration(seconds: timeout), await connect);
        }
      }

      my.addListener('update status value', (dynamic payload) {
        print('up sta va payload::${payload.toString()}');
        dynamic id = payload['id'];
        // find accessory in list and update state
        int index = accessories.indexWhere((f) => f.id == id && f.bobaos_id == my.name);
        if (index > -1) {
          void updateOneField(dynamic payload) {
            dynamic field = payload['field'];
            dynamic value = payload['value'];
            accessories[index].updateCurrentState(field, value);
            accessories[index].notifyListeners();
          }

          dynamic statusValues = payload['status'];
          if (statusValues is Map) {
            updateOneField(statusValues);
          }
          if (statusValues is List) {
            statusValues.forEach((statusValue) {
              updateOneField(statusValue);
            });
          }
        }
      });

      my.addListener("remove accessory", (dynamic payload) {
        //{"method":"remove accessory","payload":"thermostat_1"}
        void processOneAccessory(id) {
          dynamic id = payload;
          int index = accessories.indexWhere((f) => f.id == id && f.bobaos_id == my.name);
          if (index > -1) {
            accessories.removeAt(index);
            // TODO: notify listeners?
          }
        }

        if (payload is List) {
          payload.forEach((id) {
            processOneAccessory(id);
          });
        } else {
          processOneAccessory(payload);
        }
      });

      my.addListener("clear accessories", (dynamic payload) {
        accessories.removeWhere((f) => f.bobaos_id == my.name);
        // TODO: notify listeners
      });

      my.addListener("add accessory", (dynamic pl) {
        void processOneAccessory(payload) async {
          AccessoryInfo f = new AccessoryInfo(pl);
          f.bobaos_id = my.name;
          // get all status values
          dynamic payload = await my.getStatusValue(f.id, f.status);
          dynamic statusValues = payload['status'];
          if (statusValues is Map) {
            dynamic field = statusValues['field'];
            dynamic value = statusValues['value'];
            f.updateCurrentState(field, value);
          }
          if (statusValues is List) {
            statusValues.forEach((statusValue) {
              if (statusValue is Map) {
                dynamic field = statusValue['field'];
                dynamic value = statusValue['value'];
                f.updateCurrentState(field, value);
              }
            });
          }

          accessories.add(f);
        }

        if (pl is List) {
          pl.forEach((f) => processOneAccessory(f));
        } else {
          processOneAccessory(pl);
        }
      });

      my.addListener('error', (e) {
        //  connect();
      });
      my.addListener('done', connect);

      await connect();
      kits.add(my);

      // TODO: cancel subscription on disconnect
      var subscription = Connectivity().onConnectivityChanged.listen((ConnectivityResult result) async {
        // Got a new connectivity status!
        if (result == ConnectivityResult.mobile) {
          // I am connected to a mobile network.
          await connect();
        } else if (result == ConnectivityResult.wifi) {
          // I am connected to a wifi network.
          await connect();
        }
      });
    } else {
      print('already paired');
    }
  }

  events.addListener('pair', (ServicePairedInfo info) async {
    int index = pairedDevicesInfo.paired.indexWhere((s) {
      return s == info.name;
    });
    if (index == -1) {
      // pair
      await pair(info);
      await pairedDevicesInfo.add(info.name);
      info.paired = true;
      mdnsExplorer.notifyListeners();
    } else {
      print('already paired');
    }
  });
  events.addListener('unpair', (ServicePairedInfo info) async {
    int index = pairedDevicesInfo.paired.indexWhere((s) {
      return s == info.name;
    });
    if (index > -1) {
      // unpair
      // close connection, remove from paired list:
      // find in `kits` list by name, then close ws
      // find in `paired` list by name, then delete from there
      // do not forget to write to file and notify listeners
      int bindex = kits.indexWhere((BobaosKitWs bk) {
        return bk.name == info.name;
      });
      print('bobaoskit ${info.name} index is: $bindex}');
      if (bindex > -1) {
        // delete from rooms
        roomListInfo.rooms.forEach((RoomInfo f) {
          f.accessories.removeWhere((a) => a.indexOf(info.name) == 0);
          f.notifyListeners();
        });
        await kits[bindex].closeWs();
        kits.removeAt(bindex);
        accessories.removeWhere((f) => f.bobaos_id == info.name);
        await pairedDevicesInfo.remove(info.name);
        info.paired = false;
        // but how to notify mdns screen?
        mdnsExplorer.notifyListeners();
      }
    } else {
      print('already unpaired');
    }
  });

  mdnsExplorer.emitter.addListener('resolved', (ServicePairedInfo info) async {
    print('resolved listener: ${info.name}: ${info.host}:${info.port}, ${info.paired}');
    int index = pairedDevicesInfo.paired.indexWhere((s) {
      return s == info.name;
    });
    if (index > -1) {
      // pair
      await pair(info);
      info.paired = true;
      mdnsExplorer.notifyListeners();
    } else {
      print('not paired');
    }
  });

  mdnsExplorer.emitter.addListener('lost', (ServicePairedInfo info) async {
    print('lost listener: ${info.name}: ${info.host}:${info.port}, ${info.paired}');
    kits.removeWhere((f) => f.name == info.name);
  });

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.red, brightness: Brightness.dark),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return new RoomListScreen();
  }
}
